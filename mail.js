const nodemailer = require('nodemailer')

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    host: process.env.SMTP,
    port: process.env.PORT,
    secure: false, // true for 465, false for other ports
    auth: {
        user: process.env.USER,
        pass: process.env.PASS
    }
})

// setup email data with unicode symbols
let mailOptions = {
    from: process.env.USER, // sender address
    to: process.env.RECIPIENT, // list of receivers
    subject: "Hello ✔", // Subject line
    text: "Hello world?", // plain text body
    html: "<b>Hello world?</b>" // html body
}

transporter.sendMail(mailOptions,(err,info)=>{
    if (err) {
        console.log(err)
    }else{
        console.log(info)
    }
})